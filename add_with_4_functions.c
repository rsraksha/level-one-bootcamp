//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>
int entry()
{
    int num;
    printf("Enter the number:");
    scanf("%d",&num);
    return num;
}
int add(int m,int n)
{
    return m+n;
}
void print(int sum) 
{
    printf("Sum of two numbers:%d",sum);
}
int main()
{
    int a,b,sum;
    a=entry();
    b=entry();
    sum=add(a,b);
    print(sum);
    return 0;
}