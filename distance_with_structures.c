//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
struct dist
{
    float d1,d2;
}m1,m2;
float entryd1()
{
    float d1;
    scanf("%f",&d1);
    return d1;
}
float entryd2()
{
    float d2;
    scanf("%f",&d2);
    return d2;
}
void output(float distance)
{
        printf("Distance between two points is: %f\n",distance);
}
float compute(float a1,float a2,float b1,float b2)
{
    return(sqrt(pow(a2-a1,2)+pow(b2-b1,2)));
}
float main()
{
    float a1,a2,b1,b2,dis;
    printf("Enter the co-ordinates a1,a2: ");
    m1.d1=entryd1();
    m2.d1=entryd1();
    printf("Enter the co-ordinates b1,b2: ");
    m1.d2=entryd2();
    m2.d2=entryd2();
    dis=compute(m1.d1,m2.d1,m1.d2,m2.d2);
    output(dis);
    return 0;
}