//WAP to find the sum of n fractions.
#include <stdio.h>
struct fract
{
    int num;
    int dem;
} n1,n2,ans;
int input()
{
    int a;
    scanf("%d",&a);
    return a;
}
int numcalc(int a, int b, int c, int d)
{
    return ((a*d)+(b*c));
}
int demcalc(int b, int d)
{
    return (b*d);
}
int gcdcal(int a, int b)
{
    int gcd=1;
    for(int i=1; i <= a && i <= b; ++i)
    {
        if(a%i==0 && b%i==0)
            gcd = i;
    }
    return gcd;
}
int main()
{
struct fract n1,n2,ans;
int x,y,gcd;
int n;
printf("Enter N :" );
n=input();
printf("Enter the numerator for 1st number :" );
n1.num =input();
printf("Enter the denominator for 1st number : ");
n1.dem =input();
printf("\n" );
for(int i=0;i<n-1;i++)
{
    printf("Enter the numerator for next number : ");
    n2.num =input();
    printf("Enter the denominator for next number : ");
    n2.dem =input();
    printf("\n" );
    x= numcalc(n1.num,n1.dem,n2.num,n2.dem); //numerator
    y=demcalc(n1.dem,n2.dem); //denominator
    gcd = gcdcal(x,y);
    ans.num=x/gcd;
    ans.dem=y/gcd;
    n1.num=ans.num;
    n1.dem=ans.dem;
}
printf("\nThe added fraction is %d/%d" ,ans.num,ans.dem);
printf("\n");
return 0;
}

