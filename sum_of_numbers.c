//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>
int input()
{
    int num; 
    printf("Enter the total number of elements:"); 
    scanf("%d",&num);
    return num; 
}
int compute(int n,int a[n])
{ 
    int i,sum=0; 
    for(i=0;i<n;i++) 
    { 
        printf("Enter %d number:",i); 
        scanf("%d",&a[i]);
        sum=sum+a[i];
    }
    return sum;
}
void output(int sum)
{
    printf("The sum of numbers is:%d",sum);
}
int main() 
{ 
    int n,sum; 
    n=input(); 
    int num[n]; 
    sum=compute(n,num); 
    output(sum); 
    return 0; 
}