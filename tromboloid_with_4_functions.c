//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
float value()
{
    float a;
    scanf("%f",&a);
    return a;
}
float volume(float h,float b,float d)
{
    return (0.33*((h*d*b)+(d/b)));
}
void output(float vol)
{
    printf("Volume of tromboloid is:%f",vol);
}
float main()
{
    float h,b,d;
    float vol;
    printf("Enter value of height:");
    h=value();
    printf("Enter value of breadth:");
    b=value();
    printf("Enter value of depth:");
    d=value();
    vol=volume(h,b,d);
    output(vol);
    return 0;
}