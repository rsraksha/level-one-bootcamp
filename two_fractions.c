//WAP to find the sum of two fractions.
#include<stdio.h>
typedef struct
{
    int n;
    int d;
}fraction;
    fraction input()
    {
        fraction f;
        printf("Enter the numerator:");
        scanf("%d",&f.n);
        printf("Enter the denominator:");
        scanf("%d",&f.d);
        return f;
    }
    int gcd (int a  , int b)
    {
        if(a == 0)
        {
            return b;
        }
        return gcd(b % a, a);
    }
    fraction compute(fraction f1, fraction f2)
    {
    fraction sum;
    int n,d;
    n = (f1.n*f2.d)+(f2.n*f1.d);
    d = f1.d*f2.d;
    sum.n = n/gcd(n,d);
    sum.d = d/gcd(n,d);
    return sum;
    }
    void output(fraction f1, fraction f2, fraction sum)
    {
    printf("The sum of fractions %d/%d and %d/%d is %d/%d\n",f1.n,f1.d,f2.n,f2.d,sum.n,sum.d);
    }
    float main()
    {
        fraction sum;
        fraction f1,f2;
        f1 = input();
        f2 = input();
        sum = compute(f1,f2);
        output(f1,f2,sum);
        return 0;
    }